# Assessment Test from PT Investree Radhika Jaya

## Description

This project creates a website weather station which data getting from https://www.metaweather.com/api/

## Language
- HTML
- CSS
- JavaScript
- Bootstrap 4

## Installation

- Download it and paste to your local computer.
- Open your XAMPP and start Apache and MySQL service.
- Open browser and access it with url `localhost/assessmenttest`.
- Turn off ** Moesif Origins & CORS Changer ** Extension.
- Make sure your computer connected to internet.

## Directory structure

- `/assets/css` holds application code for css styles and dependencies.
- `/assets/js` holds application data, configuration and dependencies.
- `/assets/img` holds images for application.
- `index.php` main page the application.


## Requirement software

- A browser that supports html5 Google Chrome preferred.
- Web server XAMPP : https://www.apachefriends.org/download.html v.7.2 or higher.
- Extension for Google Chrome Moesif Origins & CORS Changer : https://chrome.google.com/webstore/detail/moesif-orign-cors-changer/digfbfaphojjndkpccljibejjbppifbc?utm_source=chrome-ntp-icon.
