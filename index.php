<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home - MetaWeather</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">MetaWeather</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarItem" aria-controls="navbarItem" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarItem">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#" id="home">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" id="daftarKota">Daftar Kota</a>
                </li>
            </ul>
            <form class="my-2 my-lg-0" id="searchForm" method="POST" action="submit.php">
                <div class="form-row">
                    <div class="col-12">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" name="search" id="citySearch" maxlength="20">
                    </div>
                </div>
            </form>
        </div>
    </nav>


    <div class="container weather-main">

        <!-- Current today weather -->
        <div class="row">
            <div class="col-12 pt-3 pb-3">
                <h1 class="weather-city-name text-center text-light"></h1>
                <ul class="weather-city-list text-light"></ul>
            </div>
            <div class="col-5 d-flex justify-content-end align-items-end weather-degree-today"></div>
            <div class="col-7">
                <img src="" alt="weather-stats" class="img-ws weather-image">
            </div>
            <div class="col-lg-5 col-md-5 d-flex weather-minmax-today"></div>
            <div class="col-lg-3 col-md-7 col-sm-4 offset-lg-1 d-flex p-0 justify-content-center">
                <h3 class="weather-pressure-stats text-light"></h3>
            </div>
            <div class="w-100"></div>
            <div class="col align-self-center text-center pb-4">
                <h1 class="weather-states-name text-primary">...</h1>
            </div>
        </div>

        <!-- Another stats weather  -->
        <div class="row">
            <div class="col-lg-3 offset-lg-1 col-md-4 col-sm-6">
                <div class="weather-wind-stats d-inline-flex"></div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="weather-rain-stats d-inline-flex"></div>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-6">
                <div class="weather-visibility-stats d-inline-flex"></div>
            </div>
            <div class="col-lg-2 col-md-4">
                <div class="weather-confidence-stats d-inline-flex"></div>
            </div>
        </div>

        <!-- 5 Days weather -->
        <div class="row" id="fiveDaysWeather"></div>
    </div>

    <div class="container list-city pt-5">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">City</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</body>
    <script src="assets/js/jquery-3.4.1.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/formSubmit.js"></script>
</html>
