var searchCities = [];
$(document).ready(function () {
    searchCities = []
    $('.list-city').hide();
    getDataWeather('1047378');
})
$("#daftarKota").click(function (event) {
    event.preventDefault();
    $('.weather-main').fadeOut();
    $('.list-city tbody').empty();
    var historySearchElement;
    if (typeof searchCities != "undefined" && searchCities != null && searchCities.length != null && searchCities.length > 0) {
        for (let index = 0; index < searchCities.length; index++) {
            historySearchElement = "<tr><td>" + searchCities[index] + "</td></tr>";
            $('.list-city tbody').append(historySearchElement)
        }
    } else {
        historySearchElement = "<tr><td>Data Not Found</td></tr>";
        $('.list-city tbody').append(historySearchElement)
    }
    $('.list-city').fadeIn();
});
$("#home").click(function (event) {
    event.preventDefault();
    getDataWeather('1047378');
    $('.list-city').fadeOut();
    $('.weather-main').fadeIn();
});
$("#searchForm").submit(function (event) {
    // cancels the form submission
    event.preventDefault();
    submitSearch();
});

function submitSearch() {
    var city = $('#citySearch').val();
    getDataCity(city);
    searchCities.push(city);
}
function getDataCity(id) {
    $.ajax({
        type: "GET",
        url: "https://www.metaweather.com/api/location/search/?query=" + id,
        success: function (response) {
            $('#citySearch').val('');
            $(".weather-city-list").empty();
            if (response.length === 1) {
                $('.weather-city-name').text(response[0]['title']);
                getDataWeather(response[0]['woeid']);
            } else if (response.length > 1) {
                for (let index = 0; index < response.length; index++) {
                    var listElement = "<li><a href='#' id='cities' name='" + response[index]['woeid'] + "' onclick='myFunction()'>" + response[index]['title'] + "</a></li>";
                    $(".weather-city-list").show();
                    $('.weather-city-list').append(listElement);
                    $('.weather-city-name').text('List City');
                    $('.img-ws').hide();
                    $('.weather-states-name').empty();
                    disapierElement();
                }
            } else {
                $('.weather-city-name').text('Data Not Found');
                $('.img-ws').hide();
                $('.weather-states-name').empty();
                disapierElement();
            }
        }
    });
}

function getDataWeather(woeid) {
    $.ajax({
        type: "GET",
        url: "https://www.metaweather.com/api/location/" + woeid,
        success: function (response) {
            $('.weather-city-name').text(response.title);
            disapierElement();
            $('.img-ws').show();
            for (let index = 0; index < response.consolidated_weather.length; index++) {
                var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                var date = new Date(response.consolidated_weather[index].applicable_date)
                var day = date.getDay();
                if (index >= 1) {
                    var offsetClass;
                    if (index == 1) {
                        offsetClass = "offset-lg-1";
                    } else {
                        offsetClass = "";
                    }
                    var fiveDayElement = "<div class='col-lg-2 " + offsetClass + " col-md-6 p-1'><div class='card text-white bg-dark'><div class='card-header text-center'><h6 class='text-uppercase'>" + weekday[day] + "</h6></div><div class='card-body'><div class='row'><div class='col-6'><img src='https://www.metaweather.com/static/img/weather/" + response.consolidated_weather[index].weather_state_abbr + ".svg' alt='weather-stats' class='img-ws-small'></div><div class='col-6 text-right'><p><span class='text-warning'>" + Math.round(response.consolidated_weather[index].max_temp) + "&deg;</span><br><span class='text-primary'>" + Math.round(response.consolidated_weather[index].min_temp) + "&deg;</span></p></div></div></div></div></div>"
                    $('#fiveDaysWeather').append(fiveDayElement);
                } else {
                    var weatherDegreeElement = "<h1 class='text-light'>" + Math.round(response.consolidated_weather[0].the_temp) + "&deg;</h1>";
                    var tempMinMaxElement = "<h3 class='text-primary'>" + Math.round(response.consolidated_weather[0].min_temp) + "&deg;</h3><h3 class='text-light'>/</h3><h3 class='text-warning pl-1'>" + Math.round(response.consolidated_weather[0].max_temp) + "&deg;</h3>"
                    var windSpeed = response.consolidated_weather[0].wind_speed * 1.609;
                    var windStatsElement = "<i class='fa fa-wind fa-inverse pt-1'></i><h5 class='text-primary pl-2'>Wind</h5><h5 class='text-light pl-2'>" + Math.round(windSpeed) + "<small>Km/h</small></h5><h5 class='text-light pl-2'>" + response.consolidated_weather[0].wind_direction_compass + "</h5>"
                    var rainStatsElement = "<i class='fa fa-tint fa-inverse pt-1'></i><h5 class='text-primary pl-2'>Humidity</h5><h5 class='text-light pl-2'>" + response.consolidated_weather[0].humidity + "<small>%</small></h5>"
                    var visibilityStatsElement = "<i class='fa fa-eye fa-inverse pt-1'></i><h5 class='text-primary pl-2'>Visibility</h5><h5 class='text-light pl-2'>" + Math.round(response.consolidated_weather[0].visibility) + "<small>miles</small></h5>"
                    var confidenceStatsElement = "<h5 class='text-primary pl-1'>Confidence</h5><h5 class='text-light pl-1'>" + response.consolidated_weather[0].predictability + "<small>%</small></h5>"
                    var presssureStatsElement = "Pressure <small class='text-primary'>" + Math.round(response.consolidated_weather[0].air_pressure) + "mb</small>";
                    $('.weather-degree-today').append(weatherDegreeElement)
                    $('.img-ws').attr('src', 'https://www.metaweather.com/static/img/weather/' + response.consolidated_weather[0].weather_state_abbr + '.svg');
                    $('.weather-states-name').text(response.consolidated_weather[0].weather_state_name);
                    $('.weather-pressure-stats').append(presssureStatsElement);
                    $('.weather-wind-stats').append(windStatsElement);
                    $('.weather-rain-stats').append(rainStatsElement);
                    $('.weather-visibility-stats').append(visibilityStatsElement);
                    $('.weather-confidence-stats').append(confidenceStatsElement);
                    $('.weather-minmax-today').append(tempMinMaxElement);
                }
            }
        }
    });
}

function disapierElement() {
    $('#fiveDaysWeather').empty();
    $('.weather-minmax-today').empty();
    $('.weather-wind-stats').empty();
    $('.weather-rain-stats').empty();
    $('.weather-visibility-stats').empty();
    $('.weather-confidence-stats').empty();
    $('.weather-degree-today').empty();
    $('.weather-pressure-stats').empty();
}
function myFunction() {
    var woeid = $("#cities").attr("name")
    $(".weather-city-list").empty();
    $(".weather-city-list").fadeOut();
    getDataWeather(woeid);
}